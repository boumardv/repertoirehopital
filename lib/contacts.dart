import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:repertoire_hopital/commons.dart';
import 'package:repertoire_hopital/phoneBook.dart';

class ContactDataList{
  final List<ContactData> data;
  ContactDataList(this.data);

  ContactDataList.fromJson(List<dynamic> dataJson):
    this.data = dataJson.map((contact) => ContactData.fromJson(contact)).toList();
}

class ContactData{
  var _name = "Name";
  var _number = "625485";
  var _iD;
  ContactData({String name, String number}): _name = name, _number = number, _iD = getUniqueID();

  void setContactName(var name){
    _name = name;
  }
  void setContactNumber(var number){
    _number = number;
  }

  String getName(){
    return _name;
  }

  String getNumber(){
    return _number;
  }

  int getID(){
    return _iD;
  }

  int compareTo(ContactData b){
    return _name.compareTo(b._name);
  }

  bool equals(ContactData b){
    return _iD.compareTo(b._iD);
  }

  Map<String, dynamic> toJson() => {
    'name' : _name,
    'number' : _number
  };

  ContactData.fromJson(Map<String, dynamic> data) {
    _name = data['name'];
    _number = data['number'];
    _iD = getUniqueID();
  }
}

class Contact extends StatefulWidget{
  final ContactData _contactData;
  final AsyncValueSetter<ContactData> _onContactEdit;
  final AsyncValueSetter<ContactData> _onContactDelete;

  Contact({Key key, ContactData data, AsyncValueSetter<ContactData> onContactEdit,
    AsyncValueSetter<ContactData> onContactDelete})
      : _contactData = data, _onContactEdit = onContactEdit,
        _onContactDelete = onContactDelete,
        super(key: key);

  @override
  _ContactState createState() => _ContactState();
}

class _ContactState<T extends Contact> extends State<T>{
  bool _displayEdit = false;

  @override
  Widget build(BuildContext context){
    return null;
  }

  @override
  void didUpdateWidget(Contact oldWidget){
    _displayEdit = false;
    super.didUpdateWidget(oldWidget);
  }

  Future<void> _editContact() {
    return widget._onContactEdit(widget._contactData);
  }

  Future<void >_deleteContact() {
    return widget._onContactDelete(widget._contactData);
  }
}

class CallableContact extends Contact {
  final String _numberPrefix;

  CallableContact({Key key, ContactData data, AsyncValueSetter<ContactData> onContactEdit,
    AsyncValueSetter<ContactData> onContactDelete, String numberPrefix}) :
        _numberPrefix = numberPrefix,
        super(key: key, data: data, onContactEdit: onContactEdit, onContactDelete: onContactDelete);

  @override
  _CallableState createState() => _CallableState();
}

class _CallableState extends _ContactState<CallableContact> {
  final _textStyle = TextStyle(fontSize: 18);
  final _iconColor = iconColor;
  double _rowHeight = 64.0;
  double _infoWidth = 64.0;

  @override
  Widget build(BuildContext buildContext) {
    _rowHeight = MediaQuery.of(buildContext).size.height * screenPercentSmall;
    _infoWidth = MediaQuery.of(buildContext).size.width * screenPercentLarge;
    return InkWell(
      onLongPress: () => setState((){_displayEdit = !_displayEdit;}),
      child: buildContactRow(),
    );
  }

  Widget buildContactRow() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildContactInfo(),
            buildContactCall(),
            buildContactEdit(),
          ],
        ),
      ),
    );
  }

  Widget buildContactInfo() {
    return ContactInfoWidget(
        name: widget._contactData._name + '  ' + widget._contactData.getID().toString(),
        number:  widget._numberPrefix != null ? widget._numberPrefix + widget._contactData._number :
                  widget._contactData._number,
        infoWidth: _infoWidth,
        textStyle: _textStyle,
    );
  }

  Widget buildContactCall() {
    if(_displayEdit){
      return Container();
    }
    return ContactCallWidget(
      iconColor: _iconColor,
      rowHeight: _rowHeight,
      onPressed: _callContact,
    );
  }

  void _callContact() {
    String fullNumber = widget._numberPrefix != null ? widget._numberPrefix + widget._contactData._number :
                        widget._contactData._number;
    launch('tel://' + fullNumber);
  }

  Widget buildContactEdit() {
    if(!_displayEdit){
      return SizedBox(width: MediaQuery.of(context).size.width * screenPercentLarge,);
    }
    return ContactEditWidget(
      iconColor: _iconColor,
      rowHeight: _rowHeight,
      onEditPressed: _editContact,
      onDeletePressed: _deleteContact,
    );
  }
}

class Hospital extends Contact {
  final VoidCallback _onHospitalRouteClosed;
  final ValueSetter2Params<ContactData> _onHospitalServiceAdded;
  final ValidationMethod<ContactData> _canValidateServiceAdd;
  final ContactListMethodWithParam _getContacts;

  Hospital({Key key, ContactData data, AsyncValueSetter<ContactData> onContactEdit,
    AsyncValueSetter<ContactData> onContactDelete, VoidCallback onHospitalRouteClosed,
    ValidationMethod<ContactData> canValidateServiceAdd, ValueSetter2Params<ContactData> onHospitalServiceAdded,
    ContactListMethodWithParam getContacts}) :
      _onHospitalRouteClosed = onHospitalRouteClosed, _onHospitalServiceAdded = onHospitalServiceAdded,
        _canValidateServiceAdd = canValidateServiceAdd, _getContacts = getContacts,
        super(key: key, data: data, onContactEdit: onContactEdit, onContactDelete: onContactDelete);

  @override
  _HospitalState createState() => _HospitalState();

  static int getNumberLength(){
    return 4;
  }
}

class _HospitalState extends _ContactState<Hospital> {
  var _textStyle = TextStyle(fontSize: 20, color: Colors.black);
  final _screenHeightPercent = 8.0 / 100.0;
  final _screenWidthPercent = 25.0 / 100.0;
  final _iconColor = Colors.green.shade400;
  double _rowHeight = 64.0;
  double _infoWidth = 64.0;
  bool _displayEdit = false;

  @override
  Widget build(BuildContext buildContext) {
    _rowHeight = MediaQuery.of(buildContext).size.height * _screenHeightPercent;
    _infoWidth = MediaQuery.of(buildContext).size.width * _screenWidthPercent;
    return InkWell(
            onTap: _pushHospitalRoute,
            onLongPress: () => setState((){_displayEdit = !_displayEdit;}),
            child: buildContactRow(),
          );
  }

  Widget buildContactRow() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildContactInfo(),
            buildContactEdit(),
          ],
        ),
      ),
    );
  }

  Widget buildContactInfo() {
    return ContactInfoWidget(
      name: widget._contactData._name + '  ' + widget._contactData.getID().toString(),
      number: widget._contactData._number,
      infoWidth: _infoWidth,
      textStyle: _textStyle,
    );
  }

  Widget buildContactEdit() {
    if(_displayEdit) {
      return ContactEditWidget(
        iconColor: _iconColor,
        rowHeight: _rowHeight,
        onEditPressed: _editContact,
        onDeletePressed: _deleteContact,
      );
    }
    return Container();
  }

  void _pushHospitalRoute(){
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context){
          return ContactListWidget(
            owner: widget._contactData,
            onServiceAdded: (value) => widget._onHospitalServiceAdded(value, widget._contactData),
            onContactsListChanged: getContacts,
            validateContactToAdd: widget._canValidateServiceAdd,
            onContactEdit: widget._onContactEdit,
            onContactDelete: widget._onContactDelete,
            numberPrefix: widget._contactData.getNumber(),
          );
        },
      ),
    ).then((_) {
      setState(() {widget._onHospitalRouteClosed();});
    } );
  }

  List<ContactData> getContacts() {
    return widget._getContacts(widget._contactData);
  }
}

class ContactListWidget extends StatefulWidget {
  final _contacts = <ContactData>[];
  final ContactData _owner;
  final ValueChanged<ContactData> _onServiceAdded;
  final ValueGetter<List<ContactData>> _onContactsListChanged;
  final ValidationMethod<ContactData> _validateContactToAdd;
  final AsyncValueSetter<ContactData> _onContactEdit;
  final AsyncValueSetter<ContactData> _onContactDelete;
  final String _numberPrefix;

  ContactListWidget({Key key, ContactData owner,
    ValueChanged<ContactData> onServiceAdded, ValueGetter<List<ContactData>> onContactsListChanged,
    ValidationMethod<ContactData> validateContactToAdd, AsyncValueSetter<ContactData> onContactEdit,
    AsyncValueSetter<ContactData> onContactDelete, String numberPrefix}) :
      _owner = owner, _onServiceAdded = onServiceAdded, _onContactsListChanged = onContactsListChanged,
      _validateContactToAdd = validateContactToAdd, _onContactEdit = onContactEdit,
      _onContactDelete = onContactDelete, _numberPrefix = numberPrefix,
    super(key: key) {
      List<ContactData> init = onContactsListChanged();
      if(init != null){
      _contacts.addAll(init);
      }
    }

  @override
  _ContactListWidgetState createState() =>_ContactListWidgetState();

}

class _ContactListWidgetState extends State<ContactListWidget>{
  @override
  Widget build(BuildContext buildContext) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._owner._name + ' - ' + widget._owner._number),
        actions: [
          IconButton(icon: Icon(Icons.add), onPressed: _pushAddContact)
        ],
      ),
      body: ListView.builder(
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        padding: EdgeInsets.all(1.0),
        itemBuilder: (context, index){
          if(index < widget._contacts.length) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CallableContact(
                  data: widget._contacts[index],
                  onContactEdit: _onContactEdit,
                  onContactDelete: _onContactDelete,
                  numberPrefix: widget._numberPrefix,
                ),
                Divider(height: 20.0, color: Colors.black)
              ],
            );
          }
          return null;
        }
    ),
    );
  }

  void _pushAddContact(){
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context){
          return AddContact(
            icon: Icon(Icons.person_add),
            numberLength: serviceNumLength,
            canDisplayForm: () => true,
          );
        },
      ),
    ).then((_) {
      _refresh();
    } );
  }

  Future<void> _onContactEdit(ContactData value) async {
    await widget._onContactEdit(value);
    _refresh();
  }

  Future<void> _onContactDelete(ContactData value) async {
    await widget._onContactDelete(value);
    _refresh();
  }

  void _refresh(){
    widget._contacts.clear();
    widget._contacts.addAll(widget._onContactsListChanged());
    setState(() {});
  }
}

class ContactEditWidget extends StatelessWidget {
  const ContactEditWidget({
    Key key,
    @required Color iconColor,
    @required double rowHeight,
    @required VoidCallback onEditPressed,
    @required VoidCallback onDeletePressed,
  }) : _iconColor = iconColor, _rowHeight = rowHeight,
        _onEditPressed = onEditPressed, _onDeletePressed = onDeletePressed,
        super(key: key);

  final Color _iconColor;
  final double _rowHeight;
  final VoidCallback _onEditPressed;
  final VoidCallback _onDeletePressed;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            IconButton(
                icon: Icon(Icons.edit),
                color: _iconColor,
                iconSize: _rowHeight - 2,
                onPressed: _onEditPressed,
                ),
            IconButton(
                icon: Icon(Icons.delete_forever_sharp),
                color: _iconColor,
                iconSize: _rowHeight - 2,
                onPressed: _onDeletePressed,
                ),
          ],
        ),
    );
  }
}

class ContactCallWidget extends StatelessWidget {
  const ContactCallWidget({
    Key key,
    @required Color iconColor,
    @required double rowHeight,
    @required VoidCallback onPressed,
  }) : _iconColor = iconColor, _rowHeight = rowHeight, _onPressed = onPressed, super(key: key);

  final Color _iconColor;
  final double _rowHeight;
  final VoidCallback _onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.zero,
        child:
          IconButton(
            icon: Icon(Icons.call),
            color: _iconColor,
            iconSize: _rowHeight - 2,
            onPressed: _onPressed,
          ),
    );
  }
}

class ContactInfoWidget extends StatelessWidget {
  const ContactInfoWidget({
    Key key,
    @required String name,
    @required String number,
    @required double infoWidth,
    @required TextStyle textStyle,
  }) : _infoWidth = infoWidth, _name = name, _textStyle = textStyle, _number = number, super(key: key);

  final double _infoWidth;
  final String _name;
  final TextStyle _textStyle;
  final String _number;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
      //width: _infoWidth,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            _name,
            style: _textStyle,
            textWidthBasis: TextWidthBasis.longestLine,
            textAlign: TextAlign.start,
          ),
          Text(
            _number,
            style: _textStyle,
            textWidthBasis: TextWidthBasis.longestLine,
            textAlign: TextAlign.start,
          ),
        ]
      ),
    );
  }
}
