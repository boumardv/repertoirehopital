import 'package:flutter/material.dart';

import 'contacts.dart';

Color iconColor = Colors.green.shade400;

final screenPercentSmall = 8.0 / 100.0;
final screenPercentLarge = 25.0 / 100.0;

typedef ValidationMethod<T> = bool Function(T value);
typedef ContactListMethodWithParam = List<ContactData> Function(ContactData value);
typedef ValueSetter2Params<T> = void Function(T value, T value2);

int _uniqueID = 0;
int getUniqueID() => _uniqueID++;

final int hospitalNumLength = 6;
final int serviceNumLength = 4;



class AddContactOrHospital extends StatefulWidget{
  final bool canDisplayCallableAdd;
  final bool canDisplayHospitalAdd;
  final ValueChanged<ContactData> callableDataValidated;
  final ValueChanged<ContactData> hospitalDataValidated;
  final int callableNumberLength;

  AddContactOrHospital({Key key, bool canAddCallable = true, bool canAddHospital = true,
    ValueChanged<ContactData> callableDataValidated,
    ValueChanged<ContactData> hospitalDataValidated,
    int callableNumberLength}) :
        this.canDisplayCallableAdd = canAddCallable,
        this.canDisplayHospitalAdd = canAddHospital,
        this.callableDataValidated = callableDataValidated,
        this.hospitalDataValidated = hospitalDataValidated,
        this.callableNumberLength = callableNumberLength,
        super(key: key);

  @override
  _AddContactOrHospitalState createState() => _AddContactOrHospitalState();
}

class _AddContactOrHospitalState extends State<AddContactOrHospital> {
  bool _displayCallableForm = false;
  bool _displayHospitalForm = false;

  @override
  Widget build(BuildContext context) {
    double _iconBigSize = MediaQuery.of(context).size.height * screenPercentLarge;
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Contact'),
      ),
      body: Column(children: [
        AddContact(
          icon: Icon(Icons.person_add),
          dataValidated: widget.callableDataValidated,
          onFormDisplayed: () {
            _displayCallableForm = !_displayCallableForm ;
            _displayHospitalForm = _displayCallableForm ? false : _displayHospitalForm;
            setState(() {});
          },
          canDisplayForm: () => !_displayHospitalForm && _displayCallableForm,
        ),
        AddContact(
          icon: Icon(Icons.add_business),
          dataValidated: widget.hospitalDataValidated,
          numberLength: hospitalNumLength,
          onFormDisplayed: () {
            _displayHospitalForm = !_displayHospitalForm;
            _displayCallableForm = _displayHospitalForm ? false : _displayCallableForm;
            setState(() {});
          },
          canDisplayForm: () => !_displayCallableForm && _displayHospitalForm,
        ),
      ]),
    );
  }
}

class AddContact extends StatefulWidget{
  final ValueChanged<ContactData> _dataValidated;
  final int _numberLength;
  final Icon _icon;
  final ValueGetter<bool> _canDisplayForm;
  final VoidCallback _onFormDisplayed;

  AddContact({Key key, ValueChanged<ContactData> dataValidated, int numberLength,
              Icon icon, ValueGetter<bool> canDisplayForm, VoidCallback onFormDisplayed}) :
      _dataValidated = dataValidated, _numberLength = numberLength, _icon = icon,
      _canDisplayForm = canDisplayForm, _onFormDisplayed = onFormDisplayed,
        super(key: key);

  @override
  _AddContactState createState() => _AddContactState();
}

class _AddContactState extends State<AddContact> {
  bool _displayForm = false;

  @override
  Widget build(BuildContext context) {
    double _iconBigSize = MediaQuery.of(context).size.height * screenPercentLarge;
    return Column(children: [
        _buildAdd(),
        _buildForm(),
      ]
    );
  }

  Widget _buildAdd(){
    double _iconBigSize = MediaQuery.of(context).size.height * screenPercentSmall;
    return IconButton(
      icon: widget._icon,
      iconSize: _iconBigSize,
      onPressed: () {
        setState(() {
          widget._onFormDisplayed();
        });
      },
      color: iconColor,
    );
  }

  Widget _buildForm(){
    if(widget._canDisplayForm()) {
      return AddContactForm(
        onValidate: _onValidateAdd,
        numLength: widget._numberLength,
      );
    }
    return Container();
  }

  void _onValidateAdd(ContactData data){
    Navigator.pop(context);
    widget._dataValidated(data);
  }
}

class AddContactForm extends StatefulWidget {
  const AddContactForm({
    Key key,
    @required ValueSetter<ContactData> onValidate,
    int numLength,
    ContactData baseData,
  }) :  _onValidate = onValidate, _baseData = baseData,
        _numberLength = numLength, super(key: key);

  final ValueSetter<ContactData> _onValidate;
  final ContactData _baseData;
  final int _numberLength;

  @override
  _AddContactFormState createState() => _AddContactFormState();
}

class _AddContactFormState extends State<AddContactForm>{

  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  ContactData _contactToAdd = ContactData();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _key,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          TextFormField(
            keyboardType: TextInputType.name,
            initialValue: widget._baseData != null ? widget._baseData.getName() : '',
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Name',
            ),
            validator: _validateName,
            onSaved: (value) => _contactToAdd.setContactName(value),
          ),
          TextFormField(
            keyboardType: TextInputType.phone,
            initialValue: widget._baseData != null ? widget._baseData.getNumber() : '',
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Number',
            ),
            validator: _validateNumber,
            onSaved: (value) => _contactToAdd.setContactNumber(value),
          ),
          IconButton(
            icon: Icon(Icons.save_rounded),
            color: _key.currentState != null && _key.currentState.validate() ? iconColor : Colors.black,
            onPressed: () {
              if(_key.currentState != null && _key.currentState.validate()) {
                _key.currentState.save();
                if (_contactToAdd.getName() != null &&
                    _contactToAdd.getNumber() != null) {
                  widget._onValidate(_contactToAdd);
                }
              }
            },
          )
        ],
      ),
    );
  }

  String _validateNumber(String value) {
    bool numberIsValid = value != null
        && (widget._numberLength == null || value.length == widget._numberLength);
    if(!numberIsValid){
      _contactToAdd.setContactNumber(null);
    }
    return numberIsValid ? null : 'le numero doit comporter ${widget._numberLength} chiffres';
  }


  String _validateName(String value) {
    bool nameIsValid = value != null && value.isNotEmpty;
    if(!nameIsValid){
      _contactToAdd.setContactName(null);
    }
    return nameIsValid ? null : 'le nom est vide';
  }
}