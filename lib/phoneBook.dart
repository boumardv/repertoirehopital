import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:repertoire_hopital/commons.dart';

import 'contacts.dart';

class PhoneBook extends StatefulWidget{
  final Map<String, dynamic> _data;

  PhoneBook(Map<String, dynamic> data) : _data = data;

  @override
  _PhoneBookState createState() {
    return _data != null ?
      _PhoneBookState.fromJson(_data) :
      _PhoneBookState();
  }
}

class _PhoneBookState extends State<PhoneBook>{
  final _speedDials;
  final _hospitals;
  final Map<int, List<ContactData>> _hospitalsData = Map<int, List<ContactData>>();
  bool _displayContactForm = false;
  bool _displayHospitalForm = false;

  _PhoneBookState() :
  _speedDials = <ContactData>[],
  _hospitals = <ContactData>[];

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Repertoire'),
        actions: [
          IconButton(icon: Icon(Icons.add), onPressed: _pushAddContact)
        ],
      ),
      body: buildContactList(),
    );
  }

  ListView buildContactList() {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      padding: EdgeInsets.all(1.0),
      itemBuilder: (context, index){
        if(index < _speedDials.length) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CallableContact(
                data: _speedDials[index],
                onContactEdit: _editContact,
                onContactDelete: _deleteContact,
              ),
              Divider(height: 20.0, color: Colors.black)
            ],
          );
        }
        else {
          final secondaryIndex = index - _speedDials.length;
          if(secondaryIndex < _hospitals.length){
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Hospital(
                  data: _hospitals[secondaryIndex],
                  onContactEdit: _editContact,
                  onContactDelete: _deleteContact,
                  onHospitalRouteClosed: () => setState((){}),
                  canValidateServiceAdd: validateContactToAdd,
                  onHospitalServiceAdded: _onHospitalServiceAdded,
                  getContacts: (value) => _hospitalsData[value.getID()],
                ),
                Divider(height: 20.0, color: Colors.black)
              ],
            );
          }
        }
        return null;
      }
    );
  }

  void _pushAddContact(){
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context){
          return AddContactOrHospital(
            callableDataValidated: onSpeedDialAdded,
            hospitalDataValidated: onHospitalAdded,
          );
        },
      ),
    ).then((_) {
      setState(() {
        saveInfo();
      });
    } );
  }

  void onSpeedDialAdded(ContactData data){
    _speedDials.add(data);
    _speedDials.sort((ContactData a, ContactData b) => a.compareTo(b));
  }

  void onHospitalAdded(ContactData data){
    _hospitals.add(data);
    _hospitals.sort((ContactData a, ContactData b) => a.compareTo(b));
  }

  bool validateContactToAdd(ContactData data){
    return data.getName() != null && data.getName().isNotEmpty
        && data.getNumber() != null && data.getNumber().isNotEmpty;
  }

  Future<void> _editContact(ContactData data) {
    return Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context){
            return Scaffold(
              appBar: AppBar(
                title: Text('Edit Contact ${data.getName()}'),
              ),
              body: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
                  return AddContactForm(
                    baseData: data,
                    onValidate: (value) => _replaceContactData(data, value),
                  );
                },
              ),
            );
          },
        )
    ).then((value) => setState(() {
      saveInfo();
    }));
  }

  void _replaceContactData(ContactData oldData, ContactData newData){
    ContactData hospitalData = _hospitals.isEmpty ? null :
    _hospitals.firstWhere((element) => element.getID() == oldData.getID(),
        orElse: () => null);
    ContactData speedDialData = _speedDials.isEmpty ? null :
    _speedDials.firstWhere((element) => element.getID() == oldData.getID(),
        orElse: () => null);
    ContactData serviceData;
    _hospitalsData.forEach((key, value) {
      serviceData = value.firstWhere((element) => element.getID() == oldData.getID(),
      orElse: () => serviceData);
    });

    assert(hospitalData != null || speedDialData != null || serviceData != null);

    if(hospitalData != null){
      hospitalData.setContactName(newData.getName());
      hospitalData.setContactNumber(newData.getNumber());
    }
    else if(speedDialData != null){
      speedDialData.setContactName(newData.getName());
      speedDialData.setContactNumber(newData.getNumber());
    }
    else if(serviceData != null){
      serviceData.setContactName(newData.getName());
      serviceData.setContactNumber(newData.getNumber());
    }

    Navigator.pop(context);
  }

  Future<void> _deleteContact(ContactData data){
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Delete Contact \'${data.getName()}\' ?'),
            actions: [
              Row(
                children: [
                  TextButton(
                      onPressed: () {
                        _deleteContactConfirmed(data);
                        Navigator.of(context).pop();
                        },
                      child: Text('Yes')
                  ),
                  TextButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text('No')
                  ),
                ],
              )
            ],
          );
        }
    );
  }

  void _deleteContactConfirmed(ContactData data){
    if(_hospitals.isNotEmpty){
      _hospitals.removeWhere((element) => element.getID() == data.getID());
    }
    if(_speedDials.isNotEmpty){
      _speedDials.removeWhere((element) => element.getID() == data.getID());
    }
    _hospitalsData.forEach((key, value) {
      value.removeWhere((element) => element.getID() == data.getID());
    });
    _hospitalsData.removeWhere((key, value) => key == data.getID());

    setState(() {
      saveInfo();
    });
  }

  void _onHospitalServiceAdded(ContactData data, ContactData hospital){
    _hospitalsData.update(hospital.getID(), (value) {value.add(data); return value;},
      ifAbsent: () => [data]
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['speedDials'] = jsonEncode(_speedDials);
    data['hospitals'] = jsonEncode(_hospitals);

    _hospitalsData.forEach((key, value) {
      data[key.toString()] = jsonEncode(value);
    });

    return data;
  }

  _PhoneBookState.fromJson(Map<String, dynamic> data) :
    _speedDials = ContactDataList.fromJson(jsonDecode(data['speedDials']) as List<dynamic>).data,
    _hospitals = ContactDataList.fromJson(jsonDecode(data['hospitals']) as List<dynamic>).data;


  Future<void> saveInfo() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('phonebook', jsonEncode(toJson()));
  }
}
