import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:repertoire_hopital/commons.dart';

import 'contacts.dart';
import 'phoneBook.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget  {
  Map<String, dynamic> _loadedData;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _loadData(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if(snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
              title: "Repertoire Hopital",
              theme: ThemeData(
                primaryColor: Colors.indigo,
              ),
              home: PhoneBook(_loadedData)
          );
        }

        return  MaterialApp(
            title: "Repertoire Hopital",
            theme: ThemeData(
              primaryColor: Colors.indigo,
            ),
            home: Text('caca prout <3')
        );
      },
    );
  }

  Future<bool> _loadData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String str = prefs.getString('phonebook');
    if (str != null) {
      _loadedData = jsonDecode(str) as Map<String, dynamic>;
    }
    else {
      _loadedData = null;
    }
    return true;
  }
}
